#include "asset_tools/Mesh.hpp"

#include <iostream>

namespace asset_tools {

    Vertex::Vertex(
            Vector<float, 3> const &position,
            Vector<float, 3> const &normal,
            Vector<float, 2> const texture_coordinates) throw()
        : position(position), normal(normal), texture_coordinates(texture_coordinates) {}

    Mesh::Mesh() {}

    Mesh::Mesh(aiMesh const &ai_mesh) {
        this->vertices.reserve(ai_mesh.mNumVertices);
        this->indices.reserve(ai_mesh.mNumFaces);

        if (!ai_mesh.HasNormals())
            throw NoNormalsException();

        for (size_t vertex_index = 0; vertex_index < ai_mesh.mNumVertices; vertex_index += 1) {
            Vector<float, 2> texture_coordinates = ai_mesh.HasTextureCoords(0)
                    ? Vector<float, 2>(
                              ai_mesh.mTextureCoords[0][vertex_index].x,
                              ai_mesh.mTextureCoords[0][vertex_index].y)
                    : Vector<float, 2>(0.0f, 0.0f);

            this->vertices.emplace_back(
                    Vector<float, 3>(ai_mesh.mVertices[vertex_index]),
                    Vector<float, 3>(ai_mesh.mNormals[vertex_index]), texture_coordinates);
        }

        for (size_t face_index = 0; face_index < ai_mesh.mNumFaces; face_index += 1) {
            aiFace const &ai_face = ai_mesh.mFaces[face_index];

            if (3 != ai_face.mNumIndices)
                throw NonTriangularFaceException(ai_face.mNumIndices);

            this->indices.push_back(ai_face.mIndices[0]);
            this->indices.push_back(ai_face.mIndices[1]);
            this->indices.push_back(ai_face.mIndices[2]);
        }
    }

    std::vector<Vertex> const &Mesh::get_vertices() const { return this->vertices; };

    std::vector<std::uint32_t> const &Mesh::get_indices() const { return this->indices; };

    MeshParseException::MeshParseException(std::string const &what) throw()
        : std::runtime_error(what) {}
    NoNormalsException::NoNormalsException() throw()
        : MeshParseException(
                  std::string("Could not parse the mesh as it did not contain any normals.")) {}
    NonTriangularFaceException::NonTriangularFaceException(size_t const vertices_count) throw()
        : MeshParseException(
                  std::string("Could not parse the mesh as it contained a face with ") +
                  std::to_string(vertices_count) + std::string(" vertices instead of 3.")){};
} // namespace asset_tools