#pragma once

#ifndef IG_NODE_HPP
#define IG_NODE_HPP

#include "asset_tools/Component.hpp"
#include "assimp/scene.h"
#include "msgpack.hpp"
#include <functional>
#include <vector>

namespace asset_tools {
    struct Entity {
        std::vector<ComponentVariant> components;

        Entity();

        static void extract_hierarchy(
                aiScene const &ai_scene,
                aiNode const &ai_node,
                std::function<size_t(Entity &&entity)> const &callback,
                size_t mesh_offset,
                size_t material_offset,
                std::optional<size_t> parent = std::optional<size_t>()) throw();

        MSGPACK_DEFINE(components);

      private:
        Entity(std::vector<ComponentVariant> components) throw();
    };
} // namespace asset_tools
#endif