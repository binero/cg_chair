#pragma once

#ifndef IG_VECTOR_HPP
#define IG_VECTOR_HPP

#include "assimp/vector2.h"
#include "assimp/vector3.h"
#include "msgpack.hpp"
#include "glm/vec3.hpp"
#include "glm/vec2.hpp"
#include <array>

namespace asset_tools {
    template <typename T, size_t N>
    struct Vector {
        std::array<T, N> contents;

        bool operator==(Vector<T, N> const &that) const {
            for (size_t index = 0; index < N; index += 1)
                if (!(this->contents[index] == that.contents[index]))
                    return false;
            return true;
        }

        Vector() = delete;
    };

    template <>
    struct Vector<float, 3> {
        std::array<float, 3> contents;
        Vector() throw(){};
        Vector(aiVector3D const &ai_vector) throw()
            : contents{ai_vector.x, ai_vector.y, ai_vector.z} {};
        Vector(float const x, float const y, float const z) : contents{x, y, z} {};

        bool operator==(Vector<float, 3> const &that) const {
            for (size_t index = 0; index < 3; index += 1)
                if (!(this->contents[index] == that.contents[index]))
                    return false;
            return true;
        }

        glm::vec3 to_glm() const {
            return glm::vec3(contents[0], contents[1], contents[2]);
        }

        MSGPACK_DEFINE(contents);
    };

    template <>
    struct Vector<float, 2> {
        std::array<float, 2> contents;
        Vector() throw(){};
        Vector(aiVector2D const &ai_vector) throw() : contents{ai_vector.x, ai_vector.y} {};
        Vector(float const x, float const y) : contents{x, y} {};

        bool operator==(Vector<float, 2> const &that) const {
            for (size_t index = 0; index < 2; index += 1)
                if (!(this->contents[index] == that.contents[index]))
                    return false;
            return true;
        }

        glm::vec2 to_glm() const {
            return glm::vec2(contents[0], contents[1]);
        }

        MSGPACK_DEFINE(contents);
    };
} // namespace asset_tools

namespace std {
    template <size_t N>
    struct hash<asset_tools::Vector<float, N>> {
        size_t operator()(asset_tools::Vector<float, N> const &vector) const {
            size_t hash_value = 0;

            for (size_t index = 0; index < N; index += 1)
                hash_value += std::hash<float>{}(vector.contents[index]);

            return std::hash<size_t>{}(hash_value);
        }
    };
} // namespace std

#endif