﻿#include "asset_tools/Entity.hpp"
#include "asset_tools/Material.hpp"
#include "asset_tools/Mesh.hpp"
#include "assimp/Importer.hpp"
#include "assimp/postprocess.h"
#include "msgpack.hpp"
#include <filesystem>
#include <fstream>
#include <functional>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <tuple>
#include <vector>

using namespace asset_tools;

/**
 * Extracts all meshes from a scene, and yields them to the callback one by one.
 */
size_t extract_meshes(aiScene const &ai_scene, std::function<void(Mesh &&)> const callback) {
    for (size_t mesh_index = 0; mesh_index < ai_scene.mNumMeshes; mesh_index += 1)
        callback(Mesh(*ai_scene.mMeshes[mesh_index]));
    return ai_scene.mNumMeshes;
}

/**
 * Extracts all materials from a scene, and yields them to the callback one by one.
 */
size_t extract_materials(aiScene const &ai_scene, std::function<void(Material &&)> const callback) {
    for (size_t material_index = 0; material_index < ai_scene.mNumMaterials; material_index += 1)
        callback(Material(*ai_scene.mMaterials[material_index]));
    return ai_scene.mNumMaterials;
}

/**
 * Extracts all entities from a scene, and yields them to the callback one by one.
 *
 * Because we might be parsing multiple scenes, the `_offset` parameters are used to indicate how
 * much we already extracted in previous scenes. Without these, the entities would potentially point
 * to meshes/materials/parents in other scenes.
 */
size_t extract_entities(
        aiScene const &ai_scene,
        std::function<void(Entity &&)> const callback,
        size_t const mesh_offset,
        size_t const material_offset,
        size_t const entity_offset) {
    size_t next_entity_index = entity_offset;

    auto const forward_callback = [&](Entity &&node) {
        callback(std::move(node));
        return next_entity_index++; // We return the index of the entity we just created.
    };

    Entity::extract_hierarchy(
            ai_scene, *ai_scene.mRootNode, forward_callback, mesh_offset, material_offset);

    return next_entity_index - entity_offset; // The amount of entities extracted.
}

Assimp::Importer load_importer() {
    Assimp::Importer importer;

    importer.SetPropertyInteger(
            AI_CONFIG_PP_RVC_FLAGS,
            aiComponent_TANGENTS_AND_BITANGENTS | aiComponent_BONEWEIGHTS | aiComponent_ANIMATIONS);
    importer.SetPropertyInteger(
            AI_CONFIG_PP_SBP_REMOVE, aiPrimitiveType_POINT | aiPrimitiveType_LINE);

    return importer;
}

/**
 * Loads a scene from a file, or throws an exception when this was not possible.
 */
aiScene const *
load_scene_from_file(Assimp::Importer &importer, std::filesystem::path const &file_path) {
    static unsigned const IMPORT_FLAGS = aiProcess_Triangulate | aiProcess_GenSmoothNormals |
            aiProcess_GenUVCoords | aiProcess_FindDegenerates | aiProcess_FindInstances |
            aiProcess_JoinIdenticalVertices | aiProcess_ImproveCacheLocality |
            aiProcess_OptimizeGraph | aiProcess_OptimizeMeshes |
            aiProcess_RemoveRedundantMaterials | aiProcess_ValidateDataStructure |
            aiProcess_RemoveComponent | aiProcess_FindDegenerates | aiProcess_SortByPType;

    aiScene const *const scene_ptr = importer.ReadFile(file_path.string(), IMPORT_FLAGS);

    if (nullptr == scene_ptr)
        throw std::runtime_error(importer.GetErrorString());
    else
        return scene_ptr;
}

/**
 * Copies the textures used by the material from `in_path` to `out_path`.
 */
void copy_textures(
        Material const &material,
        std::filesystem::path const &in_path,
        std::filesystem::path const &out_path) {

    if (material.diffuse_texture.has_value()) {
        std::filesystem::path in_diffuse_texture_path(in_path);
        in_diffuse_texture_path.append(*material.diffuse_texture);
        std::filesystem::path out_diffuse_texture_path(out_path);
        out_diffuse_texture_path.append(*material.diffuse_texture);

        std::filesystem::copy(in_diffuse_texture_path, out_diffuse_texture_path);
    }

    if (material.ambient_texture.has_value()) {
        std::filesystem::path in_ambient_texture_path(in_path);
        in_ambient_texture_path.append(*material.ambient_texture);
        std::filesystem::path out_ambient_texture_path(out_path);
        out_ambient_texture_path.append(*material.ambient_texture);

        std::filesystem::copy_file(in_ambient_texture_path, out_ambient_texture_path);
    }
}

/**
 *  Transpiles a file and returns the amount of meshes, materials and entities read.
 */
std::tuple<size_t, size_t, size_t> transpile_file(
        std::filesystem::path const &file_path,
        std::filesystem::path const &output_textures_path,
        std::ostream &meshes_stream,
        std::ostream &materials_stream,
        std::ostream &entities_stream,
        size_t const mesh_offset,
        size_t const material_offset,
        size_t const entity_offset) {

    std::filesystem::path resources_path = std::filesystem::path(file_path);
    resources_path.remove_filename();

    std::cerr << "Transpiling " << file_path.string() << std::endl;
    // This needs to stay in scope for at least as long as `scene`.
    Assimp::Importer importer = load_importer();
    aiScene const &scene = *load_scene_from_file(importer, file_path);

    // Extract what we need.
    std::cerr << " :: Read scene. Starting extraction." << std::endl;

    size_t const meshes_read =
            extract_meshes(scene, [&](Mesh const &&mesh) { msgpack::pack(meshes_stream, mesh); });
    std::cerr << " :: Extracted " << meshes_read << " meshes." << std::endl;

    size_t const materials_read = extract_materials(scene, [&](Material const &&material) {
        copy_textures(material, resources_path, output_textures_path);
        msgpack::pack(materials_stream, material);
    });
    std::cerr << " :: Extracted " << materials_read << " materials." << std::endl;

    size_t const entities_read = extract_entities(
            scene, [&](Entity const &&entity) { msgpack::pack(entities_stream, entity); },
            mesh_offset, material_offset, entity_offset);
    std::cerr << " :: Extracted " << entities_read << " entities." << std::endl;

    return {meshes_read, materials_read, entities_read};
}

int main(int const argument_count, char const *const arguments[]) throw() {
    size_t const file_count = argument_count - 1;

    std::ofstream meshes_stream("meshes.meal");
    std::ofstream materials_stream("materials.meal");
    std::ofstream entities_stream("entities.meal");

    size_t total_meshes_read = 0;
    size_t total_materials_read = 0;
    size_t total_entities_read = 0;

    
    std::filesystem::path const output_textures_path =
            std::filesystem::absolute(std::filesystem::path("textures"));
    std::filesystem::remove_all(output_textures_path);
    std::filesystem::create_directories(output_textures_path);
    std::cerr << "Created textures directory." << std::endl;

    bool all_success = true;
    for (size_t file_index = 0; file_index < file_count; file_index += 1) {
        std::filesystem::path const file_path(arguments[file_index + 1]);

        try {
            auto const [meshes_read, materials_read, entities_read] = transpile_file(
                    file_path, output_textures_path, meshes_stream, materials_stream, entities_stream, total_meshes_read,
                    total_materials_read, total_entities_read);
            total_meshes_read += meshes_read;
            total_materials_read += materials_read;
            total_entities_read += entities_read;
        } catch (std::exception const &exception) {
            std::cerr << " :: Could not parse file " << file_path.string() << "." << std::endl;
            std::cerr << " :: - Cause: " << exception.what() << std::endl;
            all_success = false;
        }
    }

    std::cerr << "Packaged " << total_meshes_read << " meshes, " << total_materials_read
              << " materials and " << total_entities_read << " entities." << std::endl;

    meshes_stream.close();
    materials_stream.close();
    entities_stream.close();

    return all_success ? 0 : 1;
}
