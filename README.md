# Compilation

Compilation is does using CMake. You need at least CMake version 3.10.

The project uses C++17 and was tested on the `clang` compiler. Compile and run as follows:

```sh
mkdir build
cd build
CXX=clang++ CC=clang cmake -DCMAKE_BUILD_TYPE=Release ..
make
cd Chair
./Chair
```

To successfully compile, you need to have Assimp version 4.1.0 and GLFW version 3.2.1 installed. All other dependencies are delivered in the `dependencies/` directory.

# Project Structure

This project exists out of one library and two executables.

 - **`AssetTools/`**: This library is responsible for reading and writing MEAL files, our own file format for entities, meshes and meals, based on msgpack. 
 - **`Chair/`**: This is the actual OpenGL project.
 - **`Cooker/`**: This program transpiles modules files of various assimp-supported formats into MEAL files.
 - **`dependencies/`**: Dependencies used by the projects mentioned above.
   - **`glm/`**: This is a fork of GLM, the popular OpenGL math library.
   - **`entt/`**: This is a fork of entt, a C++ Entity Component System Library.
   - **`msgpack-c/`**: This is a fork of msgpack-c, the official msgpack implementation for C and C++.
 - **`objects/`**: The assets used by this project.
 - **`CMakeLists.txt`**: The main CMake project file.
 - **`README.md`**: This file.
 - **`recording.mp4`**: A demonstration of the required features of this project.
 - **`model_sources.txt`**: A file containing the sources for each of the used models.
 - **`verslag.pdf`**: The report in which the project and design choices are discussed.