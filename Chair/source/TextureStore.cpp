#include "TextureStore.hpp"

TextureStore::TextureStore() : loaded_files(), loaded_colours() {}

std::shared_ptr<Texture> TextureStore::load_texture_from_file(std::string const &filename) {
    auto map_position = this->loaded_files.find(filename);
    
    if (map_position == this->loaded_files.cend()) {
        auto texture = Texture::from_file(filename.c_str());
        this->loaded_files.insert(
                std::pair<std::string, std::shared_ptr<Texture>>(std::string(filename), texture));
        return texture;
    } else
        return map_position->second;
}
std::shared_ptr<Texture>
TextureStore::load_texture_from_colour(asset_tools::Vector<float, 3> const colour) {
    auto map_position = this->loaded_colours.find(colour);
    if (map_position == this->loaded_colours.cend()) {
        auto texture = Texture::from_colour(colour);
        this->loaded_colours.insert(
                std::pair<asset_tools::Vector<float, 3>, std::shared_ptr<Texture>>(
                        colour, texture));
        return texture;
    } else
        return map_position->second;
}
