#include "Framebuffer.hpp"
#include "utils.hpp"
#include <array>
#include <iostream>

Framebuffer Framebuffer::initialise(unsigned int const width, unsigned int const height) {
    GLuint framebuffer;
    GLuint depth_buffer;
    std::array<GLuint, 4> textures;

    glGenFramebuffers(1, &framebuffer);
    glGenRenderbuffers(1, &depth_buffer);
    glGenTextures(4, textures.data());

    GLuint const position_texture = textures[0];
    GLuint const normals_texture = textures[1];
    GLuint const diffuse_texture = textures[2];
    GLuint const ambient_texture = textures[3];

    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);

    // configure texture 0
    glBindTexture(GL_TEXTURE_2D, position_texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB, GL_FLOAT, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(
            GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, position_texture, 0);

    // configure texture 1
    glBindTexture(GL_TEXTURE_2D, normals_texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB, GL_FLOAT, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, normals_texture, 0);

    // configure texture 2
    glBindTexture(GL_TEXTURE_2D, diffuse_texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGB, GL_FLOAT, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, diffuse_texture, 0);

    glBindTexture(GL_TEXTURE_2D, ambient_texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, GL_TEXTURE_2D, ambient_texture, 0);

    // set attachments
    std::array<GLuint, 4> const colour_attachments = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1,
                                                      GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3};
    glDrawBuffers(colour_attachments.size(), colour_attachments.data());

    glBindRenderbuffer(GL_RENDERBUFFER, depth_buffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depth_buffer);

    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        throw std::runtime_error("failed to initialise framebuffer");

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glBindTexture(GL_TEXTURE_2D, 0);

    return Framebuffer(
            framebuffer, position_texture, normals_texture, diffuse_texture, ambient_texture,
            depth_buffer);
}

Framebuffer::Framebuffer(Framebuffer &&that)
    : framebuffer(that.framebuffer), position_texture(that.position_texture),
      normal_texture(that.normal_texture), diffuse_texture(that.diffuse_texture),
      ambient_texture(that.ambient_texture), depth_buffer(that.depth_buffer) {
    that.framebuffer = 0;
    that.position_texture = 0;
    that.normal_texture = 0;
    that.diffuse_texture = 0;
    that.ambient_texture = 0;
    that.depth_buffer = 0;
}

void Framebuffer::operator=(Framebuffer &&that) {
    this->framebuffer = that.framebuffer;
    this->position_texture = that.position_texture;
    this->normal_texture = that.normal_texture;
    this->diffuse_texture = that.diffuse_texture;
    this->ambient_texture = that.ambient_texture;
    this->depth_buffer = that.depth_buffer;

    that.framebuffer = 0;
    that.position_texture = 0;
    that.normal_texture = 0;
    that.diffuse_texture = 0;
    that.ambient_texture = 0;
    that.depth_buffer = 0;
}

Framebuffer::Framebuffer(
        GLuint const framebuffer,
        GLuint const position_texture,
        GLuint const normal_texture,
        GLuint const diffuse_texture,
        GLuint const ambient_texture,
        GLuint const depth_buffer)
    : framebuffer(framebuffer), position_texture(position_texture), normal_texture(normal_texture),
      diffuse_texture(diffuse_texture), ambient_texture(ambient_texture),
      depth_buffer(depth_buffer) {}

Framebuffer::~Framebuffer() {
    glDeleteFramebuffers(1, &this->framebuffer);
    std::array<GLuint, 4> textures{this->position_texture, this->normal_texture,
                                   this->diffuse_texture, this->ambient_texture};
    glDeleteTextures(textures.size(), textures.data());
    glDeleteRenderbuffers(1, &this->depth_buffer);
}

void Framebuffer::setup_geometry_pass() const {
    glBindFramebuffer(GL_FRAMEBUFFER, this->framebuffer);
}

void Framebuffer::setup_deferred_pass() const {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, this->position_texture);
    glActiveTexture(GL_TEXTURE0 + 1);
    glBindTexture(GL_TEXTURE_2D, this->normal_texture);
    glActiveTexture(GL_TEXTURE0 + 2);
    glBindTexture(GL_TEXTURE_2D, this->diffuse_texture);
    glActiveTexture(GL_TEXTURE0 + 3);
    glBindTexture(GL_TEXTURE_2D, this->ambient_texture);
}
