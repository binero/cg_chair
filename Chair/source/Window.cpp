#include "glad.h"
#include <stdexcept>

#include "Window.hpp"

std::shared_ptr<Window> Window::initialise() {
    if (!glfwInit())
        throw std::runtime_error("Failed to initialise GLFW.");

    GLFWmonitor *monitor = glfwGetPrimaryMonitor();
    GLFWvidmode const *video_mode = glfwGetVideoMode(monitor);

    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);

    GLFWwindow *window =
            glfwCreateWindow(video_mode->width, video_mode->height, "Chair", monitor, nullptr);

    if (!window)
        throw std::runtime_error("Failed to create GLFW window.");

    // Set cursor hidden en let glfw handle the re-centering
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    return std::shared_ptr<Window>(new Window(window));
}

Window::Window(GLFWwindow *window) : window(window) {
    // Initialise OpenGL.
    glfwMakeContextCurrent(window);
    gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
}

Window::~Window() {
    glfwDestroyWindow(this->window);
    glfwTerminate();
}

bool Window::should_close() const { return glfwWindowShouldClose(this->window); }

bool Window::swap_buffers() const {
    glfwSwapBuffers(this->window);
    glfwPollEvents();
    return !glfwWindowShouldClose(this->window);
}

float Window::aspect_ratio() const {
    auto const [width, height] = this->size();
    return (float)width / (float)height;
}

std::tuple<unsigned int, unsigned int> Window::size() const {
    int width;
    int height;
    glfwGetWindowSize(this->window, &width, &height);
    return {(unsigned int)width, (unsigned int)height};
}
