#include "GpuMesh.hpp"
#include <iostream>

// TODO: debug
#include "utils.hpp"
#define GLM_ENABLE_EXPERIMENTAL
#include "glm/gtx/string_cast.hpp"

GpuMesh::GpuMesh(asset_tools::Mesh const &mesh)
    : vertex_array_object(0), vertex_buffer(0), index_buffer(0), index_count(mesh.get_indices().size()) {

    glGenVertexArrays(1, &this->vertex_array_object);
    glGenBuffers(1, &this->vertex_buffer);
    glGenBuffers(1, &this->index_buffer);

    {
        auto vertices = mesh.get_vertices();
        glBindBuffer(GL_ARRAY_BUFFER, this->vertex_buffer);
        glBufferData(
                GL_ARRAY_BUFFER, sizeof(asset_tools::Vertex) * vertices.size(), vertices.data(),
                GL_STATIC_DRAW);
    }

    {
        auto indices = mesh.get_indices();
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->index_buffer);
        glBufferData(
                GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * indices.size(), indices.data(),
                GL_STATIC_DRAW);
    }

    glBindVertexArray(vertex_array_object);

    glBindBuffer(GL_ARRAY_BUFFER, this->vertex_buffer);
    glVertexAttribPointer(
            0, 3, GL_FLOAT, GL_FALSE, sizeof(asset_tools::Vertex), nullptr);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(
            1, 3, GL_FLOAT, GL_FALSE, sizeof(asset_tools::Vertex),
            (void *)sizeof(asset_tools::Vector<float, 3>));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(
            2, 2, GL_FLOAT, GL_FALSE, sizeof(asset_tools::Vertex),
            (void *)sizeof(asset_tools::Vector<float, 6>));
    glEnableVertexAttribArray(2);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->index_buffer);

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

GpuMesh::~GpuMesh() {
    std::array<GLuint, 2> buffers{this->vertex_buffer, this->index_buffer};

    glDeleteVertexArrays(1, &this->vertex_array_object);
    glDeleteBuffers(buffers.size(), buffers.data());
}

void GpuMesh::draw() const {
    glBindVertexArray(this->vertex_array_object);
    glDrawElements(GL_TRIANGLES, this->index_count, GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);
}

void GpuMesh::bind_to_compute() const {
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, this->index_buffer);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, this->vertex_buffer);
}

GpuMesh::GpuMesh(GpuMesh &&that)
    : vertex_array_object(that.vertex_array_object), vertex_buffer(that.vertex_buffer),
      index_buffer(that.index_buffer), index_count(that.index_count) {
    that.index_buffer = 0;
    that.vertex_buffer = 0;
    that.vertex_array_object = 0;
    that.index_count = 0;
}

void GpuMesh::operator=(GpuMesh &&that) {
    std::array<GLuint, 2> buffers{this->vertex_buffer, this->index_buffer};
    glDeleteVertexArrays(1, &this->vertex_array_object);
    glDeleteBuffers(2, buffers.data());
    this->vertex_array_object = that.vertex_array_object;
    this->vertex_buffer = that.vertex_buffer;
    this->index_buffer = that.index_buffer;
    this->index_count = that.index_count;
    that.index_buffer = 0;
    that.vertex_buffer = 0;
    that.vertex_array_object = 0;
    that.index_count = 0;
}
