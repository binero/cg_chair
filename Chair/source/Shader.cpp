#include "Shader.hpp"
#include "utils.hpp"
#include <iostream>

bool Shader::compilation_was_success() const {
    GLint success;
    glGetShaderiv(shader_object, GL_COMPILE_STATUS, &success);
    return success == 1;
}

void Shader::print_compilation_log() const {
    GLint log_length;
    glGetShaderiv(this->shader_object, GL_INFO_LOG_LENGTH, &log_length);

    if (log_length > 0) {
        std::unique_ptr<GLchar> log(new GLchar[log_length]);
        glGetShaderInfoLog(this->shader_object, log_length, nullptr, log.get());
        std::cerr << log.get() << std::endl;
    }
}

Shader::Shader(std::string const &shader_source, GLuint const shader_type)
    : shader_object(glCreateShader(shader_type)) {

    {
        char const *shader_source_c_str(shader_source.c_str());
        glShaderSource(this->shader_object, 1, &shader_source_c_str, nullptr);
        glCompileShader(this->shader_object);
    }

    this->print_compilation_log();

    if (!this->compilation_was_success())
        throw std::runtime_error("failed to compile shader");
}

Shader::~Shader() { glDeleteShader(this->shader_object); }

Shader::Shader(Shader &&that) : shader_object(that.shader_object) { that.shader_object = 0; };

void Shader::operator=(Shader &&that) {
    glDeleteShader(this->shader_object);
    this->shader_object = that.shader_object;
    that.shader_object = 0;
}

Shader::Shader(std::filesystem::path const &shader_path, GLuint const shader_type)
    : Shader(read_file_to_string(shader_path), shader_type) {}
GLuint Shader::get_raw_object_handle() const { return this->shader_object; }

VertexShader::VertexShader(std::filesystem::path const &path) : Shader(path, GL_VERTEX_SHADER) {}
VertexShader::VertexShader(std::string const &shader_source)
    : Shader(shader_source, GL_VERTEX_SHADER) {}

GeometryShader::GeometryShader(std::filesystem::path const &path)
    : Shader(path, GL_GEOMETRY_SHADER) {}
GeometryShader::GeometryShader(std::string const &shader_source)
    : Shader(shader_source, GL_GEOMETRY_SHADER) {}

FragmentShader::FragmentShader(std::filesystem::path const &path)
    : Shader(path, GL_FRAGMENT_SHADER) {}
FragmentShader::FragmentShader(std::string const &shader_source)
    : Shader(shader_source, GL_FRAGMENT_SHADER) {}

ComputeShader::ComputeShader(std::filesystem::path const &path) : Shader(path, GL_COMPUTE_SHADER) {}
ComputeShader::ComputeShader(std::string const &shader_source)
    : Shader(shader_source, GL_COMPUTE_SHADER) {}