#include "LightManager.hpp"
#include "Camera.hpp"
#include "Transformation.hpp"
#include <algorithm>
#include <cstring>
#include <iostream>

template <typename Entity>
LightManager<Entity> LightManager<Entity>::initialise(entt::Registry<Entity> &registry) {
    GLuint lights_ubo;
    glGenBuffers(1, &lights_ubo);

    return LightManager(lights_ubo, registry);
}

template <typename Entity>
void LightManager<Entity>::init_lights() {
    // head-mounted lamp
    this->registry.template assign<Light>(
            this->registry.template attachee<Camera>(), glm::vec3(1.0f, 1.0f, 1.0f), 1.5f);

    this->registry.template create<LocalTransformation, Light>(
            LocalTransformation(
                    glm::vec3(-150.0f, 40.0f, 350.0f), glm::vec3(0.0f, 0.0f, 0.0f),
                    glm::vec3(0.0f, 0.0f, 0.0f)),
            Light(glm::vec3(0.3f, 0.3f, 1.0f), 1.0f));
    this->registry.template create<LocalTransformation, Light>(
            LocalTransformation(
                    glm::vec3(25.0f, 350.0f, 320.0f), glm::vec3(0.0f, 0.0f, 0.0f),
                    glm::vec3(0.0f, 0.0f, 0.0f)),
            Light(glm::vec3(1.0f, 1.0f, 0.2f), 9.0f));

    { // Sun.
        Light sunlight(glm::vec3(1.0f, 0.7f, 0.7f), 100.0f);
        sunlight.toggle();

        Entity const sun = this->registry.template create<LocalTransformation, Light>(
                LocalTransformation(
                        glm::vec3(0.0f, 400.0f, 350.0f), glm::vec3(0.0f, 0.0f, 0.0f),
                        glm::vec3(0.0f, 0.0f, 0.0f)),
                std::move(sunlight));
        registry.template attach<SunLightTag>(sun);
    }
}

template <typename Entity>
void LightManager<Entity>::populate_ubo() {
    this->lights.clear();
    auto const lights_view = this->registry.template view<Light, Transformation>();

    for (auto const entity : lights_view) {
        if (this->lights.size() >= MAX_AMOUNT_LIGHTS) {
            std::cerr << "Max amount of lights added." << std::endl;
            break;
        }

        Transformation const &transformation = this->registry.template get<Transformation>(entity);
        glm::vec3 const translation = transformation.translation();
        Light const &light = this->registry.template get<Light>(entity);

        this->lights.push_back(LightShaderData(translation, light));
    }
}

template <typename Entity>
void LightManager<Entity>::bind() {
    this->populate_ubo();

    glBindBuffer(GL_UNIFORM_BUFFER, this->lights_ubo);
    glBufferData(
            GL_UNIFORM_BUFFER, MAX_AMOUNT_LIGHTS * sizeof(LightShaderData), this->lights.data(),
            GL_DYNAMIC_DRAW);
    glBindBufferBase(GL_UNIFORM_BUFFER, 4, this->lights_ubo);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
    glUniform1i(0, this->lights.size());
}

template <typename Entity>
LightManager<Entity>::~LightManager() {
    glDeleteBuffers(1, &this->lights_ubo);
}
template <typename Entity>
LightManager<Entity>::LightManager(LightManager<Entity> &&that)
    : registry(that.registry), lights_ubo(that.lights_ubo), lights(std::move(that.lights)) {
    that.lights_ubo = 0;
}

template class LightManager<std::uint32_t>;