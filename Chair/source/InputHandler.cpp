#include "InputHandler.hpp"
#include "Camera.hpp"
#include "LightStructs.hpp"
#include "RelationalComponents.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <tuple>

template <typename Entity>
InputHandler<Entity>
InputHandler<Entity>::initialise(Window *const window_wrap, RayCaster<Entity> &ray_caster) {
    double last_xpos;
    double last_ypos;
    glfwGetCursorPos(window_wrap->window, &last_xpos, &last_ypos);

    return InputHandler(window_wrap, last_xpos, last_ypos, ray_caster);
}

template <typename Entity>
InputHandler<Entity>::InputHandler(
        Window *const window_wrap,
        double const last_xpos,
        double const last_ypos,
        RayCaster<Entity> &ray_caster)
    : window_wrap(window_wrap), last_xpos(last_xpos), last_ypos(last_ypos), ray_caster(ray_caster) {

    this->register_toggle_handler(
            GLFW_KEY_1, [](entt::Registry<Entity> &registry, Renderer &renderer, Entity player) {
                registry.template get<Light>(player).toggle();
            });

    this->register_toggle_handler(
            GLFW_KEY_2, [](entt::Registry<Entity> &registry, Renderer &renderer, Entity player) {
                auto const &sun = registry.template attachee<SunLightTag>();
                registry.template get<Light>(sun).toggle();
            });

    this->register_toggle_handler(
            GLFW_KEY_L, [](entt::Registry<Entity> &registry, Renderer &renderer, Entity player) {
                renderer.set_wireframe_mode_active(!renderer.is_wireframe_mode_active());
            });

    this->register_toggle_handler(
            GLFW_KEY_P, [](entt::Registry<Entity> &registry, Renderer &renderer, Entity player) {
                renderer.set_flat_mode_active(!renderer.is_flat_mode_active());
            });
}

template <typename Entity>
void InputHandler<Entity>::register_key_to_check(int key) {
    // Binary search to find insertion position, avoid duplicates.
    auto const key_iter =
            std::lower_bound(this->keys_to_check.begin(), this->keys_to_check.end(), key);

    if (key_iter == this->keys_to_check.end() || *key_iter != key)
        this->keys_to_check.emplace(key_iter, key);
}

template <typename Entity>
void InputHandler<Entity>::register_toggle_handler(int key, ToggleHandler<Entity> const handler) {
    this->register_key_to_check(key);
    this->held_keys.insert_or_assign(key, false);

    { // Add handler.
        auto insert_iter = this->toggle_handlers.find(key);
        if (insert_iter == this->toggle_handlers.end())
            this->toggle_handlers.emplace(key, std::vector{handler});
        else
            insert_iter->second.push_back(handler);
    }
}

template <typename Entity>
void InputHandler<Entity>::enact_all(entt::Registry<Entity> &registry, Renderer &renderer) {
    auto const &player = registry.template attachee<Camera>();
    Transformation &camera_transformation = registry.template get<Transformation>(player);
    LocalTransformation &camera_local_transformation =
            registry.template get<LocalTransformation>(player);

    // Fire handlers.
    for (auto const key : this->keys_to_check) {
        bool &pressed = this->held_keys.find(key)->second;
        bool const new_pressed = glfwGetKey(this->window_wrap->window, key) == GLFW_PRESS;

        if (!pressed && new_pressed)
            for (auto handler : this->toggle_handlers.find(key)->second)
                handler(registry, renderer, player);

        pressed = new_pressed;
    }

    this->enact_mouse_click(camera_transformation, registry);
    this->enact_mouse_movement(camera_local_transformation.rotation);
    this->enact_walk_movement(
            camera_transformation.matrix, camera_local_transformation.translation);
}

template <typename Entity>
void InputHandler<Entity>::enact_mouse_movement(glm::vec3 &axis_rotations) {
    double cur_xpos;
    double cur_ypos;
    glfwGetCursorPos(this->window_wrap->window, &cur_xpos, &cur_ypos);
    // TODO: in case we support a non-fullscreen window glfwGetWindowPos has to be
    // added to cur_pos to get the correct window coords.

    this->tilt_y_axis(axis_rotations, (float)(this->last_xpos - cur_xpos));
    this->tilt_x_axis(axis_rotations, (float)(this->last_ypos - cur_ypos));

    this->last_xpos = cur_xpos;
    this->last_ypos = cur_ypos;
}

template <typename Entity>
void InputHandler<Entity>::enact_walk_movement(
        glm::mat4 const &camera_model_matrix, glm::vec3 &translation) const {
    if (glfwGetKey(this->window_wrap->window, GLFW_KEY_W) == GLFW_PRESS)
        this->forward(camera_model_matrix, translation);

    if (glfwGetKey(this->window_wrap->window, GLFW_KEY_A) == GLFW_PRESS)
        this->left(camera_model_matrix, translation);

    if (glfwGetKey(this->window_wrap->window, GLFW_KEY_S) == GLFW_PRESS)
        this->backward(camera_model_matrix, translation);

    if (glfwGetKey(this->window_wrap->window, GLFW_KEY_D) == GLFW_PRESS)
        this->right(camera_model_matrix, translation);

    if (glfwGetKey(this->window_wrap->window, GLFW_KEY_C) == GLFW_PRESS)
        this->down(camera_model_matrix, translation);

    if (glfwGetKey(this->window_wrap->window, GLFW_KEY_SPACE) == GLFW_PRESS)
        this->up(camera_model_matrix, translation);
}

template <typename Entity>
void InputHandler<Entity>::forward(
        glm::mat4 const &camera_model_matrix, glm::vec3 &translation) const {
    translation -= glm::vec3(
                           camera_model_matrix[2][0], camera_model_matrix[2][1],
                           camera_model_matrix[2][2]) *
            (float)SPEED;
}

template <typename Entity>
void InputHandler<Entity>::backward(
        glm::mat4 const &camera_model_matrix, glm::vec3 &translation) const {
    translation += glm::vec3(
                           camera_model_matrix[2][0], camera_model_matrix[2][1],
                           camera_model_matrix[2][2]) *
            (float)SPEED;
}

template <typename Entity>
void InputHandler<Entity>::left(
        glm::mat4 const &camera_model_matrix, glm::vec3 &translation) const {
    translation -= glm::vec3(
                           camera_model_matrix[0][0], camera_model_matrix[0][1],
                           camera_model_matrix[0][2]) *
            (float)SPEED;
}

template <typename Entity>
void InputHandler<Entity>::right(
        glm::mat4 const &camera_model_matrix, glm::vec3 &translation) const {
    translation += glm::vec3(
                           camera_model_matrix[0][0], camera_model_matrix[0][1],
                           camera_model_matrix[0][2]) *
            (float)SPEED;
}

template <typename Entity>
void InputHandler<Entity>::up(glm::mat4 const &camera_model_matrix, glm::vec3 &translation) const {
    translation += glm::vec3(
                           camera_model_matrix[1][0], camera_model_matrix[1][1],
                           camera_model_matrix[1][2]) *
            (float)SPEED;
}

template <typename Entity>
void InputHandler<Entity>::down(
        glm::mat4 const &camera_model_matrix, glm::vec3 &translation) const {
    translation -= glm::vec3(
                           camera_model_matrix[1][0], camera_model_matrix[1][1],
                           camera_model_matrix[1][2]) *
            (float)SPEED;
}

template <typename Entity>
void InputHandler<Entity>::tilt_y_axis(glm::vec3 &axis_rotations, float const angle) const {
    axis_rotations.y += angle * SENSITIVITY;
}

template <typename Entity>
void InputHandler<Entity>::tilt_x_axis(glm::vec3 &axis_rotations, float const angle) const {
    axis_rotations.x += angle * SENSITIVITY;
}

template <typename Entity>
void InputHandler<Entity>::enact_mouse_click(
        Transformation const &camera_transformation, entt::Registry<Entity> &registry) {
    if (glfwGetMouseButton(this->window_wrap->window, GLFW_MOUSE_BUTTON_1) == GLFW_PRESS &&
        this->mouse_1_pressable) {
        this->mouse_1_pressable = false;
        this->enact_lmb(camera_transformation, registry);
    } else if (glfwGetMouseButton(this->window_wrap->window, GLFW_MOUSE_BUTTON_1) == GLFW_RELEASE)
        this->mouse_1_pressable = true;

    if (glfwGetMouseButton(this->window_wrap->window, GLFW_MOUSE_BUTTON_2) == GLFW_PRESS &&
        this->mouse_2_pressable) {
        this->mouse_2_pressable = false;
        this->enact_rmb(camera_transformation, registry);
    } else if (glfwGetMouseButton(this->window_wrap->window, GLFW_MOUSE_BUTTON_2) == GLFW_RELEASE)
        this->mouse_2_pressable = true;
}

template <typename Entity>
void InputHandler<Entity>::enact_lmb(
        Transformation const &camera_transformation, entt::Registry<Entity> &registry) const {
    int window_width_pixels;
    int window_height_pixels;
    glfwGetFramebufferSize(this->window_wrap->window, &window_width_pixels, &window_height_pixels);
    float const aspect_ratio = this->window_wrap->aspect_ratio();

    glm::vec3 const look_ray = this->ray_caster.calc_look_ray(
            camera_transformation, window_width_pixels, window_height_pixels, aspect_ratio);

    std::tuple<Entity, bool, float> const hit_entity =
            this->ray_caster.cast(camera_transformation, look_ray);

    if (std::get<1>(hit_entity))
        registry.template get<LocalTransformation>(std::get<0>(hit_entity)).translation +=
                glm::vec3(2.0f, 0.0f, 0.0f);
}

template <typename Entity>
void InputHandler<Entity>::enact_rmb(
        Transformation const &camera_transformation, entt::Registry<Entity> &registry) const {
    int window_width_pixels;
    int window_height_pixels;
    glfwGetFramebufferSize(this->window_wrap->window, &window_width_pixels, &window_height_pixels);
    float const aspect_ratio = this->window_wrap->aspect_ratio();

    glm::vec3 const look_ray = this->ray_caster.calc_look_ray(
            camera_transformation, window_width_pixels, window_height_pixels, aspect_ratio);

    std::tuple<Entity, bool, float> const hit_entity =
            this->ray_caster.cast(camera_transformation, look_ray);

    if (std::get<1>(hit_entity)) {
        registry.template accommodate<LocalTransformation>(
                std::get<0>(hit_entity),
                LocalTransformation(
                        glm::vec3(0.0f, -100.0f, -20.0f), glm::vec3(30.0f, 0.0f, 0.0f),
                        glm::vec3(1.0f, 1.0f, 1.0f)));
        registry.template accommodate<Parent<Entity>>(
                std::get<0>(hit_entity), registry.template attachee<Camera>());
    }
}

template class InputHandler<std::uint32_t>;