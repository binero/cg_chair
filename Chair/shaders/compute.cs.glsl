#version 450

const float EPSILON = 0.0000001;

// floats are used instead of vec3 to force std430 to pack everything without padding
struct Vertex {
    float position_x, position_y, position_z;
    // following are unneeded for the computations but are still part of the struct
    float normal_x, normal_y, normal_z;
    float tex_x, tex_y;
};

layout (binding = 0, std430) buffer IndexBuff { uint index_buff[]; };
layout (binding = 1, std430) buffer VertexBuff { Vertex vertex_buff[]; };
layout (binding = 2, std430) buffer OutBuff { float distances[]; };

// max is determined by host system on which the shader is compiled
layout (location = 0) uniform float FLOAT_MAX;
layout (location = 1) uniform uint max_triangles;
layout (location = 2) uniform vec3 camera_pos;
layout (location = 3) uniform vec3 look_ray;
layout (location = 4) uniform mat4 transformation;

layout (local_size_x = 64) in;

// Look for intersection with triangle, using Moller-Trumbore algo, and save distance to it
void main() {
    uint nth_triangle = gl_GlobalInvocationID.x;
    if (nth_triangle >= max_triangles) return;

    uint base_index_0 = index_buff[(nth_triangle * 3) + 0];
    uint base_index_1 = index_buff[(nth_triangle * 3) + 1];
    uint base_index_2 = index_buff[(nth_triangle * 3) + 2];

    vec3 raw_vertex_0 = vec3(
        vertex_buff[base_index_0].position_x,
        vertex_buff[base_index_0].position_y,
        vertex_buff[base_index_0].position_z);
    vec3 raw_vertex_1 = vec3(
        vertex_buff[base_index_1].position_x,
        vertex_buff[base_index_1].position_y,
        vertex_buff[base_index_1].position_z);
    vec3 raw_vertex_2 = vec3(
        vertex_buff[base_index_2].position_x,
        vertex_buff[base_index_2].position_y,
        vertex_buff[base_index_2].position_z);

    vec3 vertex_0 = vec3(transformation * vec4(raw_vertex_0, 1.0));
    vec3 vertex_1 = vec3(transformation * vec4(raw_vertex_1, 1.0));
    vec3 vertex_2 = vec3(transformation * vec4(raw_vertex_2, 1.0));

    vec3 edge_0 = vertex_1 - vertex_0;
    vec3 edge_1 = vertex_2 - vertex_0;

    vec3 p_vec = cross(look_ray, edge_1);
    float det = dot(edge_0, p_vec);
    if (det > -EPSILON && det < EPSILON) { distances[nth_triangle] = FLOAT_MAX; return; }

    float inv_det = 1.0 / det;
    vec3 t_vec = camera_pos - vertex_0;
    float u = inv_det * dot(t_vec, p_vec);
    if (u < 0.0 || u > 1.0) { distances[nth_triangle] = FLOAT_MAX; return; }

    vec3 q_vec = cross(t_vec, edge_0);
    float v = inv_det * dot(look_ray, q_vec);
    if (v < 0.0 || u + v > 1.0) { distances[nth_triangle] = FLOAT_MAX; return; }

    float t = inv_det * dot(edge_1, q_vec);
    if (t <= EPSILON) { distances[nth_triangle] = FLOAT_MAX; return; }

    distances[nth_triangle] = t; return;
}
