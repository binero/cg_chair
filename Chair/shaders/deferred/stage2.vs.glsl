#version 450 core

layout (location = 0) in vec2 in_position;

layout (location = 0) out vec2 out_texture_coord;

void main() {
    gl_Position = vec4(in_position, 0.0, 1.0);
    out_texture_coord = (in_position.xy + 1.0) / 2.0;
}
