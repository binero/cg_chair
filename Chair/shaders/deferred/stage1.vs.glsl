#version 450 core

layout(location = 0) uniform mat4 view;
layout(location = 1) uniform mat4 projection;
layout(location = 2) uniform mat4 model;

layout(location = 0) in vec3 in_position;
layout(location = 1) in vec3 in_normal;
layout(location = 2) in vec2 in_texture_coords;

layout(location = 0) out vec3 out_position;
layout(location = 1) out vec3 out_normal;
layout(location = 2) out vec2 out_texture_coords;

void main() {
    // Convert everything from model coordinates to world coordinates.
    vec4 position = model * vec4(in_position, 1.0);
    vec4 normal = model * vec4(in_normal, 0.0);

    gl_Position = projection * view * position;

    out_position = position.xyz / position.w;
    out_normal = normal.xyz;
    out_texture_coords = in_texture_coords;
}
