#version 450 core

layout (binding = 0) uniform sampler2D diffuse_texture;
layout (binding = 1) uniform sampler2D ambient_texture;

layout(location = 0) in vec3 in_position;
layout(location = 1) in vec3 in_normal;
layout(location = 2) in vec2 in_texture_coordinates;

layout(location = 0) out vec3 out_position;
layout(location = 1) out vec3 out_normal;
// .a component of diffuse is specular intensity
layout(location = 2) out vec4 out_diffuse_specular;
layout(location = 3) out vec3 out_ambient;

void main() {
    out_position = in_position;
    out_normal = in_normal;
    out_diffuse_specular = vec4(texture(diffuse_texture, in_texture_coordinates).rgb, 1.0);
    out_ambient = texture(ambient_texture, in_texture_coordinates).rgb;
}
