#ifndef CHAIR_CHAIR_CONFIG_HPP
#define CHAIR_CHAIR_CONFIG_HPP

#include <filesystem>

namespace config {
    using std::filesystem::path;

    path const ASSETS_PATH("../assets");
    path const TEXTURES_PATH = path(ASSETS_PATH).append("textures");
    path const MESHES_FILE_PATH = path(ASSETS_PATH).append("meshes.meal");
    path const MATERIALS_FILE_PATH = path(ASSETS_PATH).append("materials.meal");
    path const ENTITIES_FILE_PATH = path(ASSETS_PATH).append("entities.meal");
} // namespace config

#endif