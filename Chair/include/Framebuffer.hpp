#ifndef CHAIR_FRAMEBUFFER_HPP
#define CHAIR_FRAMEBUFFER_HPP

#include "glad.h"

class Framebuffer {
    GLuint framebuffer;
    GLuint position_texture;
    GLuint normal_texture;
    GLuint diffuse_texture;
    GLuint ambient_texture;
    GLuint depth_buffer;

    Framebuffer(
            GLuint framebuffer,
            GLuint position_texture,
            GLuint normal_texture,
            GLuint diffuse_texture,
            GLuint ambient_texture,
            GLuint depth_buffer);
    Framebuffer(Framebuffer const &) = delete;

  public:
    static Framebuffer initialise(unsigned int width, unsigned int height);

    void setup_geometry_pass() const;
    void setup_deferred_pass() const;

    Framebuffer(Framebuffer &&);
    ~Framebuffer();
    void operator=(Framebuffer &&);
};

#endif // CHAIR_FRAMEBUFFER_HPP
