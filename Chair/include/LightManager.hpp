#ifndef CHAIR_LIGHTMANAGER_HPP
#define CHAIR_LIGHTMANAGER_HPP

#include "LightStructs.hpp"
#include "entt/entity/registry.hpp"
#include "glad.h"
#include <vector>

size_t const MAX_AMOUNT_LIGHTS = 64;

template <typename Entity>
class LightManager {
    // create
    LightManager(GLuint const lights_ubo, entt::Registry<Entity> &registry)
        : registry(registry), lights_ubo(lights_ubo) {}

    // properties
    entt::Registry<Entity> &registry;
    GLuint lights_ubo;
    std::vector<LightShaderData> lights;

    // methods
    void populate_ubo();
    LightManager(LightManager const &) = delete;

  public:
    // create
    static LightManager<Entity> initialise(entt::Registry<Entity> &registry);
    LightManager(LightManager<Entity> &&);
    ~LightManager();

    // methods
    void init_lights();
    void bind();
};

#endif // CHAIR_LIGHTMANAGER_HPP