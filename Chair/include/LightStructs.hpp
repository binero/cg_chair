#ifndef CHAIR_LIGHTSTRUCTS_HPP
#define CHAIR_LIGHTSTRUCTS_HPP

#include <algorithm>
#include <glm/vec3.hpp>

// The Light component
struct Light {
    Light(glm::vec3 const &colour, float const intensity)
        : colour(colour), intensity(intensity), last_intensity(intensity > 0.0f ? 0.0f : 1.0f) {}
    Light()
        : colour(glm::vec3(1.0f, 1.0f, 1.0f)), intensity(1.0f),
          last_intensity(intensity > 0.0f ? 0.0f : 1.0f) {}

    void toggle() {
        float const tmp = last_intensity;
        last_intensity = intensity;
        intensity = tmp;
    }

    glm::vec3 colour;
    float intensity;

  private:
    float last_intensity;
};

// The Light struct used to pass light data to shaders
class LightShaderData {
    // padding_f fixes the offset to be the same as in the shaders
    glm::vec3 position;
    float _padding_f;
    glm::vec3 colour;
    float intensity;

  public:
    LightShaderData()
        : position(glm::vec3(0.0f, 0.0f, 0.0f)), colour(glm::vec3(0.0f, 0.0f, 0.0f)),
          intensity(0.0f) {}
    LightShaderData(glm::vec3 const &position, Light const &light)
        : position(position), colour(light.colour), intensity(light.intensity) {}

    bool operator==(LightShaderData &right) {
        return this->position == right.position && this->colour == right.colour &&
                this->intensity == right.intensity;
    }

    bool operator!=(LightShaderData &right) {
        return this->position != right.position || this->colour != right.colour ||
                this->intensity != right.intensity;
    }

    LightShaderData operator=(LightShaderData right) {
        this->position = right.position;
        this->colour = right.colour;
        this->intensity = right.intensity;

        return *this;
    }
};

/// A tag for the sun.
struct SunLightTag {};

#endif // CHAIR_LIGHTSTRUCTS_HPP
