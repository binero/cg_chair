#ifndef CHAIR_UTILS_HPP
#define CHAIR_UTILS_HPP

#include <filesystem>
#include <vector>
#include <string>

void print_gl_errors();

/**
 * Reads the given file into a byte vector.
 */
std::vector<char> read_file(std::filesystem::path const &path);
std::string read_file_to_string(std::filesystem::path const &path);

#endif // CHAIR_UTILS_HPP
