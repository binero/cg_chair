#ifndef CHAIR_SHADERPROGRAM_HPP
#define CHAIR_SHADERPROGRAM_HPP

#include "Shader.hpp"
#include "glad.h"
#include <vector>

class ShaderProgram {
    GLuint shader_program_object;

    void print_linkage_log() const;
    bool linkage_was_success() const;

    void link(std::vector<Shader const *> const &) const;

  public:
    void operator=(ShaderProgram &&that);
    ShaderProgram(ShaderProgram &&that);
    ShaderProgram(VertexShader const &vertex_shader, FragmentShader const &fragment_shader);
    ShaderProgram(
            VertexShader const &vertex_shader,
            GeometryShader const &geometry_shader,
            FragmentShader const &fragment_shader);
    ShaderProgram(ComputeShader const &compute_shader);

    ~ShaderProgram();

    void use() const;
};

#endif // CHAIR_SHADERPROGRAM_HPP
