#ifndef CHAIR_FPSCOUNTER_HPP
#define CHAIR_FPSCOUNTER_HPP

#include <chrono>
using namespace std::chrono;

class FpsCounter {
    unsigned int frames = 0;
    time_point<system_clock> last_check;

    FpsCounter(time_point<system_clock> const &last_check) : last_check(last_check) {}

  public:
    static FpsCounter initialise();
    void handle_frame();
};

#endif // CHAIR_FPSCOUNTER_HPP