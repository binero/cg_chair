#ifndef CHAIR_MATERIAL_HPP
#define CHAIR_MATERIAL_HPP

#include "Texture.hpp"
#include "TextureStore.hpp"
#include "glad.h"
#include <array>
#include <assimp/material.h>
#include <glm/vec3.hpp>

class Material {
    GLuint uniform_buffer_object;

    std::shared_ptr<Texture> diffuse_texture;
    std::shared_ptr<Texture> ambient_texture;

    Material(
            GLuint uniform_buffer_object,
            std::shared_ptr<Texture> diffuse_texture,
            std::shared_ptr<Texture> ambient_texture);
    Material(Material const &) = delete;

  public:
    static Material from_assimp(aiMaterial const &ai_material, TextureStore &textures);
    void bind() const;

    Material(Material &&);
    ~Material();
};

#endif // CHAIR_MATERIAL_HPP
