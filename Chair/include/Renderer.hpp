#ifndef CHAIR_RENDERER_HPP
#define CHAIR_RENDERER_HPP

#include "Camera.hpp"
#include "Framebuffer.hpp"
#include "GpuMaterial.hpp"
#include "GpuMesh.hpp"
#include "LightManager.hpp"
#include "RayCaster.hpp"
#include "ShaderProgram.hpp"
#include "Transformation.hpp"
#include "entt/entity/registry.hpp"
#include <cstdint>
#include <memory>

float const Z_NEAR = 0.001f;
float const Z_FAR = 1000.0f;
float const FOV_DEGREE = 120.0f;

class Window;

class MeshComponent {
    friend class Renderer;
    friend class RayCaster<std::uint32_t>;
    std::vector<GpuMesh>::size_type id;
    MeshComponent(std::vector<GpuMesh>::size_type);
};

class MaterialComponent {
    friend class Renderer;
    std::vector<MaterialComponent>::size_type id;
    MaterialComponent(std::vector<MaterialComponent>::size_type);
};

class Renderer {
    std::shared_ptr<Window> window;

    ShaderProgram geometry_pass;
    ShaderProgram flat_geometry_pass;
    ShaderProgram deferred_pass;

    Framebuffer framebuffer;

    GLuint quad_vbo;
    GLuint quad_vao;

    std::vector<GpuMaterial> materials;

    bool wireframe_mode_active;
    bool flat_mode_active;

    Renderer(
            std::shared_ptr<Window> window,
            ShaderProgram geometry_pass,
            ShaderProgram flat_geometry_pass,
            ShaderProgram deferred_pass,
            Framebuffer framebuffer,
            GLuint quad_vbo,
            GLuint quad_vao);

    Renderer(Renderer const &) = delete;

  public:
    // re-usable to allow raycaster access
    std::vector<GpuMesh> meshes;

    static Renderer initialise(std::shared_ptr<Window> const window);
    template <typename Entity>
    bool render_frame(entt::Registry<Entity> &registry, LightManager<Entity> &light_manager);

    MeshComponent add_mesh(GpuMesh &&);
    MaterialComponent add_material(GpuMaterial &&);

    static glm::mat4 const calc_view_matrix(Transformation const &camera_transformation);
    static glm::mat4 const calc_perspective_matrix(float const aspect_ratio);

    size_t material_count() const;
    size_t mesh_count() const;

    bool is_wireframe_mode_active() const;
    bool is_flat_mode_active() const;
    void set_wireframe_mode_active(bool wireframe_mode_active);
    void set_flat_mode_active(bool flat_mode_active);

    Renderer(Renderer &&);
    ~Renderer();
};

#endif // CHAIR_RENDERER_HPP
